# gpt_multiplayers_game

A multi-players D&D style text adventure game based on OpenAI's assistant API.

The game comprises of two OpenAI assistants and then integrate with the image generation and the text-to-speech API. 
Of course, you will require an OpenAI API key and credits to run it.


[![](http://img.youtube.com/vi/9rSJEb9v1EQ/0.jpg)](https://youtu.be/9rSJEb9v1EQ "video")

tested: 

windows 11, python 3.10.13 with conda

Amazon Linux 2023 AMI, python 3.9.16

packages: flask openai flask-socketio cryptography requests

please refer the requirements.txt file for the detail

## setup
You need a OpenAI API key for running this project

1. clone the project
```
git clone https://gitlab.com/open_source_side_projects/gpt_multiplayers_game.git
```
2. install packages
```
pip install -r requirements.txt
```
3.  OpenAI API KEY setup
```
python init_openai.py
```
the initialization code will:
* ask your API key and encrypt it and then store it in a local file.
* ask the language you wanna use, for example, you could enter one of these (繁體中文, 日本語, English)
* generate 2 assistant by the OpenAI API.


you could check the code in init_openai.py if you concern about the key security

4. run the server
```
python server.py
```

5. visit the web page http://127.0.0.1:5000 (or your server) to play the game.

## How to play
The "Action" button (or Shift + Enter key) executes user actions.

The "Chat" button (or Enter key)is used solely for sending messages.

By clicking the dice icon in the top-right corner, you can generate a random value when requested by the
Dungeon Master. it still need to push the Action button after the value generated.


### The initial step 

* to modify your User name. ex. Bob

* followed by sending an action to describe your character, such as **"male human bard"**

### multiple players
The default number of players is set to 1.

You can adjust this by using the "SET_USER_COUNT_TO" command (e.g., SET_USER_COUNT_TO:2).

SET_USER_COUNT_TO is 2 means the DM keeps waiting until 2 different users enter their actions.

After you adjust the number of players, just share the url to your friends.

When everyone finishs the initial step, the game will automatically start.

**of course, you need an accessable ip for a multiple players game.** 

**I have used an AWS EC2 server to test it. (do not forgot to open the port 5000)**

## references:

https://codepen.io/njmcode/pen/moyjrb

https://codepen.io/richwertz/pen/XgYLzr


## TODO
* user management
* integrate with the stable video diffusion?
* integrate with a music generation project like musicgen?
