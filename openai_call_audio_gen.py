import time
import requests
import os
import json

global voice_name

voice_name = "nova"
# voice_name = "onyx"


def call_local_restful(data_str, endpoint, url="127.0.0.1:5000"):
    url = f"http://{url}/{endpoint}"
    post_json = {"mp3_filename": data_str}
    print(post_json)
    headers = {"Content-type": "application/json", "Accept": "text/plain"}
    r = requests.post(url, data=json.dumps(post_json), headers=headers)
    print(r.text)


def speech_gen(client, prompt_string):
    os.makedirs("static/gen_voice", exist_ok=True)
    # Save the image with a timestamp as its name
    """
    speech_file_path = (
        Path(__file__).parent
        + "/static/gen_voice/voice_"
        + str(int(time.time()))
        + ".mp3"
    )
    """
    filename = f"static/gen_voice/voice_{int(time.time())}.mp3"
    print("start gen audio")
    response = client.audio.speech.create(
        model="tts-1", voice=voice_name, input=prompt_string
    )

    response.stream_to_file(filename)
    call_local_restful(filename.split("/")[-1], "change_mp3")
