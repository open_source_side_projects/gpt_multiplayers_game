from cryptography.fernet import Fernet
import socket
import base64


def encrypt_api_key(api_key):
    # Generate a key and instantiate a Fernet object
    key = base64.urlsafe_b64encode(socket.gethostname().encode("utf-8").ljust(32)[:32])
    cipher_suite = Fernet(key)

    # Encrypt the API key
    byte_api_key = api_key.encode("utf-8")
    encrypted_api_key = cipher_suite.encrypt(byte_api_key)
    # save the encrypted_api_key to .key file
    with open(".key_file", "wb") as key_file:
        key_file.write(encrypted_api_key)

    return encrypted_api_key


def decrypt_api_key(encrypted_api_key, key):
    # Instantiate a Fernet object with the provided key
    key = base64.urlsafe_b64encode(socket.gethostname().encode("utf-8").ljust(32)[:32])
    cipher_suite = Fernet(key)

    # Decrypt the API key
    decrypted_api_key = cipher_suite.decrypt(encrypted_api_key)

    return decrypted_api_key


def load_key_file_and_decrypt():
    with open(".key_file", "rb") as key_file:
        encrypted_api_key = key_file.read()

    key = base64.urlsafe_b64encode(socket.gethostname().encode("utf-8").ljust(32)[:32])
    cipher_suite = Fernet(key)

    decrypted_api_key = cipher_suite.decrypt(encrypted_api_key)

    return decrypted_api_key.decode("utf-8")


if __name__ == "__main__":
    api_key = input("Enter your API key: ")
    encrypted_api_key = encrypt_api_key(api_key)
    print("Encrypted API key:", encrypted_api_key)
    key = base64.urlsafe_b64encode(socket.gethostname().encode("utf-8").ljust(32)[:32])
    print("Key:", key)

    decrypted_api_key = decrypt_api_key(encrypted_api_key, key)
    print("Decrypted API key:", decrypted_api_key.decode("utf-8"))
