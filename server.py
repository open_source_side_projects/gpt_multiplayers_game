from flask import Flask, render_template, jsonify
from flask_socketio import SocketIO, emit
from flask import request
import base64
import openai_call
from threading import Thread
import time
import os
import glob
import openai_call_doc_analyze

app = Flask(__name__)
socketio = SocketIO(app)
global action_cmd_doc
action_cmd_doc = {}

global user_count
user_count = 1
global latest_image_filename
latest_image_filename = "placeholder.jpg"

global latest_team_image_filename
latest_team_image_filename = "team_placeholder.jpg"

# Example latest MP3 filename
global latest_mp3_filename
latest_mp3_filename = ""

# global previous_return_str
# previous_return_str = ""


@app.route("/latest_mp3")
def latest_mp3():
    global latest_mp3_filename
    return jsonify(filename=latest_mp3_filename)


@app.route("/change_mp3", methods=["POST"])
def play():
    global latest_mp3_filename
    filename = request.json["mp3_filename"]

    if not os.path.exists("static/gen_voice/" + filename):
        return jsonify({"error": "File not found"}), 404
    latest_mp3_filename = filename
    return jsonify({"filename": filename})


def checking_action_cmd_ready():
    global action_cmd_doc
    global user_count
    while True:
        if len(action_cmd_doc) == user_count:
            # print(action_cmd_doc)
            action_cmd_str = ""
            for key in action_cmd_doc:
                action_cmd_str += key + ":" + action_cmd_doc[key] + "\n"
            print("action_cmd_str", action_cmd_str)
            openai_call.set_return_message("in progress...")
            openai_call.set_user_message(action_cmd_str)
            action_cmd_doc = {}

        else:
            time.sleep(1)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/set_latest_image_filename", methods=["POST"])
def set_latest_image_filename():
    global latest_image_filename
    data = request.json
    latest_image_filename = data["filename"]
    return "Image Filename Set", 200


@app.route("/set_latest_team_image_filename", methods=["POST"])
def set_latest_team_image_filename():
    global latest_team_image_filename
    data = request.json
    latest_team_image_filename = data["filename"]
    return "Image Filename Set", 200


@app.route("/image_gen", methods=["GET", "POST"])
def manual_image_gen():
    global latest_image_filename

    if request.method == "POST":
        text = request.form["text"]
        filename = openai_call.image_gen(text)
        # Update the latest image filename

        latest_image_filename = filename

    return render_template("image_gen.html", image_url=latest_image_filename)


@app.route("/team_image_gen", methods=["GET", "POST"])
def manual_team_image_gen():
    global latest_team_image_filename

    if request.method == "POST":
        text = request.form["text"]
        filename = openai_call.team_image_gen(text)
        # Update the latest image filename

        latest_team_image_filename = filename

    return render_template("image_gen.html", image_url=latest_team_image_filename)


@socketio.on("message")
def handle_message(message):
    emit("message", message, broadcast=True)


@app.route("/user_action", methods=["POST"])
def user_action():
    global user_count
    global action_cmd_doc
    data = request.json
    if "SET_USER_COUNT_TO:" in data["text"]:
        user_count = int(data["text"].split(":")[1])
        openai_call.set_user_count(user_count)
        print("set user_count to", user_count)
        return "Action Received", 200

    action_cmd_doc[data["name"]] = data["text"]
    print(action_cmd_doc)
    # Process the data as needed
    return "Action Received", 200


@app.route("/server_text_update")
def server_text_update():
    # global previous_return_str
    ret_str = openai_call.get_return_message()
    if ret_str != "":
        # previous_return_str = ret_str
        ret_str = ret_str.replace("\n", "<br>").replace("**", "")

        return {"latest_message": ret_str}
    else:
        return {}


@app.route("/latest_image_name")
def latest_image_name():
    # Logic to get the latest image name
    return {"latest_name": latest_image_filename}


@app.route("/latest_team_image_name")
def latest_team_image_name():
    # Logic to get the latest image name
    return {"latest_name": latest_team_image_filename}


@app.route("/get_image")
def get_image():
    # Load init image
    with open("static//" + latest_image_filename, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read()).decode()

    return {"image_data": encoded_string}


@app.route("/get_team_image")
def get_team_image():
    # Load init image
    with open("static//" + latest_team_image_filename, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read()).decode()

    return {"image_data": encoded_string}


if __name__ == "__main__":
    # if the path static/gen_img has any file, delete them
    for file in glob.glob("static/gen_img/*"):
        os.remove(file)
    for file in glob.glob("static/gen_voice/*"):
        os.remove(file)
    openai_call.set_user_count(user_count)
    # if openai_call_doc_analyze.lock_file_name existed, delete it
    if os.path.exists(openai_call_doc_analyze.lock_file_name):
        os.remove(openai_call_doc_analyze.lock_file_name)

    thread = Thread(target=openai_call.openai_client_main_loop)
    thread.start()
    check_action_thread = Thread(target=checking_action_cmd_ready)
    check_action_thread.start()

    socketio.run(app, debug=True, host="0.0.0.0")
