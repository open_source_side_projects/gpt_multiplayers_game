
/** 
 * Set up custom dice-roll element 
 *
 **/

const getRandomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

const rollDie = (max, numEl, perc) => {
    const newVal = getRandomInt(1, max - 1);
    numEl.textContent = newVal + (perc ? '%' : '');
};

const animateDie = (max, numEl, perc) => {
    for (let i = 0; i < 10; i++) {
        setTimeout(() => {
            requestAnimationFrame(() => rollDie(max, numEl, perc));
        }, i * 20);
    }
};

class DiceRoll extends HTMLElement {
    constructor() {
        super();
        const shadow = this.attachShadow({ mode: 'open' });

        shadow.innerHTML = `
      <div class="die">
        <span class="die-num"></span>
        <span class="die-label">
          d${this.attributes.sides.value}
        </span>
      </div>

      <style>
        .die {
          align-items: center;
          border: 1px solid #333;
          cursor: pointer;
          display: flex;
          justify-content: center;
          position: relative;
          width: 100px;
          height: 100px;
        }

        .die-num {
          color: white;
          font-family: serif;
          font-size: 36px;
          -webkit-text-stroke-width: 1px; /* Width of the stroke */
          -webkit-text-stroke-color: lightblue; /* Color of the stroke */
        }

        .die-label {
          background-color: #333;
          bottom: 0;
          color: #999;
          font-size: 14px;
          padding: 3px;
          position: absolute;
          right: 0;
        }
      </style>
    `;
        this.targetInput = document.getElementById(this.getAttribute('target-input'));
        this.numEl = shadow.querySelector('.die-num');
        this.addEventListener('click', this.roll);
        //this.roll();
    }

    roll = (e) => {
        if (e) e.preventDefault();
        animateDie(this.getAttribute('sides'), this.numEl, this.hasAttribute('percent'));

        setTimeout(() => {
            if (this.targetInput) {
                this.targetInput.value = this.numEl.textContent;
            }
        }, 200);
    }
}

window.customElements.define('dice-roll', DiceRoll);

