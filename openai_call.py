from openai import OpenAI
import encrypt_tool
from threading import Thread
import time
import os
import openai_call_doc_analyze
import openai_call_img_gen
import openai_call_audio_gen
import init_openai

global user_message
user_message = ""

global return_message
return_message = ""

global client
global assistant_id

global set_assistant_str

global user_count

a_id, a_analysis_id, use_lang = init_openai.load_assistant_ids()
set_assistant_str = a_id


def set_user_count(count):
    global user_count
    user_count = count


def del_assistant():
    global client
    global assistant_id
    client.beta.assistants.delete(assistant_id)


def set_return_message(meg):
    global return_message
    return_message = meg


def get_return_message():
    global return_message
    return return_message


def set_user_message(message):
    global user_message
    user_message = message


def openai_client_main_loop():
    global user_message
    global return_message
    global client
    global assistant_id
    global user_count

    analyze_lock_file_name = openai_call_doc_analyze.get_lock_file_name()
    client = OpenAI(api_key=encrypt_tool.load_key_file_and_decrypt())
    assistant = client.beta.assistants.retrieve(assistant_id=set_assistant_str)
    assistant_id = assistant.id

    thread = client.beta.threads.create()

    while True:
        if user_message == "":
            print("no input, waiting")
            time.sleep(5)
            continue

        # user_message = input()
        if user_message == "exit":
            break

        message = client.beta.threads.messages.create(
            thread_id=thread.id,
            role="user",
            content=user_message,
        )

        run = client.beta.threads.runs.create(
            thread_id=thread.id,
            assistant_id=assistant.id,
        )

        print("checking assistant status. ")
        while True:
            run = client.beta.threads.runs.retrieve(thread_id=thread.id, run_id=run.id)

            if run.status == "completed":
                print("done!")
                messages = client.beta.threads.messages.list(thread_id=thread.id)
                user_message = ""
                print("messages: ")

                for message in messages:
                    print(
                        {
                            "role": message.role,
                            "message": message.content[-1].text.value,
                        }
                    )
                    return_message = message.content[-1].text.value
                    # audio (all text)
                    audio_str = return_message.replace("\n", "")
                    openai_call_audio_gen.speech_gen(client, audio_str)

                    # check the lock file existed or not
                    if analyze_lock_file_name != "":
                        if os.path.exists(analyze_lock_file_name):
                            pass
                        else:
                            analysis_str = (
                                return_message.replace("\n", "")
                                .replace("{", "")
                                .replace("}", "")
                                .replace("[", "")
                                .replace("]", "")
                            )
                            openai_call_doc_analyze.gen_img_by_text_analysis(
                                analysis_str, user_count
                            )

                    # only print the latest one
                    break

                break
            elif run.status == "failed":
                print("failed!")
                return_message = "API failed!, please enter again"
                break
            elif run.status == "cancelled":
                print("cancelled!")
                return_message = "API cancelled!, please enter again"
                break
            elif run.status == "expired":
                print("expired!")
                return_message = "API expired!, please enter again"
                break
            else:
                print("in progress...", run.status)
                time.sleep(5)

    client.beta.assistants.delete(assistant.id)


def image_gen(prompt_string):
    global client
    return openai_call_img_gen.image_gen(client, prompt_string)


def team_image_gen(prompt_string):
    global client
    return openai_call_img_gen.team_image_gen(client, prompt_string)
