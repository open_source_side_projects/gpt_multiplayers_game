from openai import OpenAI
import encrypt_tool
from threading import Thread
import time
import os
import json
import openai_call_img_gen
import init_openai

# import openai_call_audio_gen

global analysis_client
global analysis_assistant_id
global lock_file_name
lock_file_name = "analysis_client.locked"
global analyze_count
analyze_count = 0


def get_lock_file_name():
    global lock_file_name
    return lock_file_name


def gen_img_by_text_analysis(input_text, user_count, target="auto"):
    global analysis_client
    global analyze_count
    global lock_file_name

    # create the lock file in disk
    if not os.path.exists(lock_file_name):
        with open(lock_file_name, "w") as f:
            f.write("1")
            f.close()
            print("create lock file: " + lock_file_name)

    print("analysis input_text: ", input_text)
    res_json = analysis_text(input_text.replace("\n", ""))
    """    
    if "Scene Description" in res_json and res_json["Scene Description"] != "":
        openai_call_audio_gen.speech_gen(analysis_client, res_json["Scene Description"])
    """

    if target == "auto":
        if analyze_count == 0:
            target = "both"
        else:
            target = "scene"

    if target == "character":
        if (
            "Character Appearance Description" in res_json
            and res_json["Character Appearance Description"] != ""
        ):
            openai_call_img_gen.set_img_char_prefix(
                res_json["Character Appearance Description"]
            )
            openai_call_img_gen.team_image_gen(
                analysis_client,
                res_json["Character Appearance Description"],
                user_count,
            )
    elif target == "scene":
        if "Scene Description" in res_json and res_json["Scene Description"] != "":
            openai_call_img_gen.image_gen(
                analysis_client, res_json["Scene Description"]
            )

    elif target == "both":
        if (
            "Character Appearance Description" in res_json
            and res_json["Character Appearance Description"] != ""
        ):
            openai_call_img_gen.set_img_char_prefix(
                res_json["Character Appearance Description"]
            )

            openai_call_img_gen.team_image_gen(
                analysis_client,
                res_json["Character Appearance Description"],
                user_count,
            )
        if "Scene Description" in res_json and res_json["Scene Description"] != "":
            openai_call_img_gen.image_gen(
                analysis_client, res_json["Scene Description"]
            )

    # delete the lock file
    os.remove(lock_file_name)
    print("delete lock file: " + lock_file_name)
    analyze_count = analyze_count + 1


def analysis_text(input_text):
    global analysis_client
    global analysis_assistant_id
    a_id, a_analysis_id, use_lang = init_openai.load_assistant_ids()
    analysis_assistant_id = a_analysis_id
    analysis_client = OpenAI(api_key=encrypt_tool.load_key_file_and_decrypt())
    # Game Text Analyzer
    analysis_assistant = analysis_client.beta.assistants.retrieve(
        assistant_id=analysis_assistant_id
    )
    # assistant_id = analysis_assistant.id

    thread = analysis_client.beta.threads.create()

    message = analysis_client.beta.threads.messages.create(
        thread_id=thread.id,
        role="user",
        content="please use " + use_lang + "to response. " + input_text,
    )
    run = analysis_client.beta.threads.runs.create(
        thread_id=thread.id,
        assistant_id=analysis_assistant.id,
    )
    print("checking assistant status. ")
    while True:
        run = analysis_client.beta.threads.runs.retrieve(
            thread_id=thread.id, run_id=run.id
        )

        if run.status == "completed":
            print("done!")
            messages = analysis_client.beta.threads.messages.list(thread_id=thread.id)

            print("messages: ")
            for message in messages:
                assert message.content[-1].type == "text"
                print({"role": message.role, "message": message.content[-1].text.value})

                res = message.content[-1].text.value
                res_json_str = (
                    res.replace("json\n", "")
                    .replace("\\n", "")
                    .replace("\n", "")
                    .replace("```", "")
                )
                try:
                    res_json = json.loads(res_json_str)
                # if the string is not a valid json, return empty json
                except:
                    res_json = {}

                return res_json
        else:
            print("in progress...")
            time.sleep(5)


if __name__ == "__main__":
    input_doc = """"""
    # res_json = analysis_text(input_doc)

    analysis_res = """"""
    # gen_img_by_text_analysis(analysis_res)
    # print("result", res_json)
