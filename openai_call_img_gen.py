import time
import requests
import os
import json

global img_style_prefix

img_style_prefix = "a D&D style,"

global img_char_prefix
img_char_prefix = ""


def set_img_char_prefix(prefix):
    global img_char_prefix
    img_char_prefix = prefix


def call_local_restful(data_str, endpoint, url="127.0.0.1:5000"):
    url = f"http://{url}/{endpoint}"
    post_json = {"filename": data_str}
    print(post_json)
    headers = {"Content-type": "application/json", "Accept": "text/plain"}
    r = requests.post(url, data=json.dumps(post_json), headers=headers)
    print(r.text)


def image_gen(client, prompt_string):
    global img_style_prefix
    global img_char_prefix
    prompt_str = (
        img_style_prefix
        + " a scene, "
        + prompt_string
        + ",if characters are existed in the image, they are: "
        + img_char_prefix
    )
    print("prompt : ", prompt_str)
    response = client.images.generate(
        model="dall-e-3",
        prompt=prompt_str,
        size="1792x1024",
        quality="standard",
        n=1,
    )

    image_url = response.data[0].url

    # Download the image from the URL
    response = requests.get(image_url)
    if response.status_code == 200:
        # Create a directory for images if it doesn't exist
        os.makedirs("static/gen_img", exist_ok=True)
        # Save the image with a timestamp as its name
        filename = f"static/gen_img/image_{int(time.time())}.png"
        with open(filename, "wb") as f:
            f.write(response.content)
        call_local_restful(filename.replace("static/", ""), "set_latest_image_filename")
        return filename.replace("static/", "")
    return ""


def team_image_gen(client, prompt_string, user_count):
    global img_style_prefix
    prompt_str = (
        str(user_count)
        + " persons in a team, "
        + img_style_prefix
        + ". "
        + prompt_string
    )
    print("prompt:  ", prompt_str)
    response = client.images.generate(
        model="dall-e-3",
        prompt=prompt_str,
        size="1024x1024",
        quality="standard",
        n=1,
    )

    image_url = response.data[0].url

    # Download the image from the URL
    response = requests.get(image_url)
    if response.status_code == 200:
        # Create a directory for images if it doesn't exist
        os.makedirs("static/gen_img", exist_ok=True)
        # Save the image with a timestamp as its name
        # filename = f"static/gen_img/team_image.png"
        filename = f"static/gen_img/team_image_{int(time.time())}.png"
        with open(filename, "wb") as f:
            f.write(response.content)
        call_local_restful(
            filename.replace("static/", ""), "set_latest_team_image_filename"
        )
        return filename.replace("static/", "")
    return ""


if __name__ == "__main__":
    print(
        call_local_restful("gen_img/image_1700635833.png", "set_latest_image_filename")
    )
    print(
        call_local_restful("gen_img/team_image.png", "set_latest_team_image_filename")
    )
