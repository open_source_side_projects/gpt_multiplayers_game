from openai import OpenAI
import json
import encrypt_tool


def create_assistant_and_save(use_language):
    if use_language == "":
        use_language = "English"
        print("use_language:", use_language)
    client = OpenAI(api_key=encrypt_tool.load_key_file_and_decrypt())
    assistant = client.beta.assistants.create(
        name="GEN infinite RPG Team Adventure",
        instructions="please use "
        + use_language
        + " to response, this is a rich text-based adventure game with three main event types: Information Gathering, Exploration, and Combat. In Information Gathering, multiple players interact with multiple NPCs for clues and assistance. During Exploration, NPCs may accompany players, providing insights, with frequent minor combat encounters. These minor combats feature relatively weaker enemies, while medium or larger battles match or exceed player strength. Enemies range from monsters to human characters or teams, with powerful monsters in major battles. When enemies appear, the GPT will vividly describe their appearance. When players perform critical actions like attack, resistance, or convincing others, the GPT sets the action's difficulty level between 2 and 19. Players are then asked to provide a number from 1 to 100. This number is multiplied by a random value and the result is taken modulo 20. If the final number is greater than the difficulty level, the action succeeds; otherwise, it fails. This system adds an element of chance to the game, enhancing the gameplay experience. The game starts with each player's character background and traits. Player actions or dialogues like 'involved in conspiracy', 'Attacked by unknown group', or 'search for clues' receive detailed outcomes, including: 1. Action consequences. 2. Scene transitions. 3. Actionable choices for the next move. Responses are at least 50 words, ensuring a deep and engaging experience.",
        model="gpt-4-1106-preview",
        tools=[],
    )
    assistant_id = assistant.id
    print("assistant_id:", assistant_id)

    analysis_assistant = client.beta.assistants.create(
        name="GEN Game Text Analyzer",
        instructions="please use "
        + use_language
        + """ to response, GEN Game Text Analyzer is now streamlined to focus exclusively on processing and classifying sentences from text-based game paragraphs into a JSON formatted document. It specializes in categorizing sentences into 'Character Appearance Description', 'Character Action', 'Scene Description', or 'User Selection'.  the "Character Appearance Description" describes the Character Appearance only. the "Scene Description" usually describe an environment, NPC, and enemies. the "User Selection" describe the next step the users can do. The output will be solely the structured JSON document, presenting each category as a key, with the relevant sentences as the value in a string format. Multiple sentences in the same Categories will be merged. Categories without sentences will appear in the JSON with an empty string value. The analyzer assumes context for ambiguous sentences to ensure a cohesive and accurate JSON output. It encourages users to provide detailed game paragraphs for precise categorization, maintaining a user-friendly and respectful interaction without additional commentary or analysis outside the JSON document. the JSON document should not include character "\n", "{","}","[","]".""",
        model="gpt-3.5-turbo-1106",
    )
    analysis_assistant_id = analysis_assistant.id
    print("analysis_assistant_id:", analysis_assistant_id)
    assistant_id_json = {
        "assistant_id": assistant_id,
        "analysis_assistant_id": analysis_assistant_id,
        "use_language": use_language,
    }

    with open(".assistant_ids", "w") as f:
        json.dump(assistant_id_json, f)
        f.close()
        print("assistant_id and analysis_assistant_id saved to .assistant_ids file")


def load_assistant_ids():
    with open(".assistant_ids", "r") as f:
        assistant_id_json = json.load(f)
        assistant_id = assistant_id_json["assistant_id"]
        analysis_assistant_id = assistant_id_json["analysis_assistant_id"]
        use_language = assistant_id_json["use_language"]
        f.close()
        print("assistant_id and analysis_assistant_id loaded from .assistant_ids file")
        return assistant_id, analysis_assistant_id, use_language


if __name__ == "__main__":
    print("Step 1: generate OpenAI API key file")
    api_key = input("Enter your API key: ")
    encrypted_api_key = encrypt_tool.encrypt_api_key(api_key)
    print("Step 2: create the OpenAI assistant")
    use_language = input("Enter the language you want to use (default: English): ")
    create_assistant_and_save(use_language)
    print("Done")
